import asyncio
import time
import websockets
from haversine import haversine

KNTS_TO_KMH = 1 / 1.852

coordinates = []
instant_speeds = []

async def handler(websocket):
    while True:
        try:
            message = await websocket.recv()
            split_message = message.split("\t")

            # Stores received coordinate as (lat, long, timestamp)
            coordinates.append((float(split_message[0]), float(split_message[1]), time.time()))
            count = len(coordinates)
            if count > 1:
                time_elapsed = coordinates[count - 1][2] - coordinates[count - 2][2]

                # Using haversine formula to compute distance
                distance = haversine((coordinates[count - 2][0], coordinates[count - 2][1]),
                    (coordinates[count - 1][0], coordinates[count - 1][1]))
                instant_speeds.append(round(3600 * distance / time_elapsed, 4))
                avg_kmh = round(sum(instant_speeds) / len(instant_speeds), 4)
                print(f'Instantaneous: {instant_speeds[count - 2]} km/h '
                      f'({round(instant_speeds[count - 2] * KNTS_TO_KMH , 4)} knots)\t'
                      f'| Average: {avg_kmh} km/h '
                      f'({round(avg_kmh * KNTS_TO_KMH, 4)} knots)')
        except websockets.ConnectionClosedOK:
            break

start_server = websockets.serve(handler, "localhost", 8000)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
