import asyncio
import time
import websockets

waypoint_file = open('missionwaypoints.txt', 'r')
waypoint_lines = waypoint_file.readlines()
waypoints = []

for line in waypoint_lines:
    waypoints.append(line.strip())


async def gps_stream():
    async with websockets.connect("ws://localhost:8000") as websocket:
        for counter in range(len(waypoints)):
            await websocket.send(waypoints[counter])
            await asyncio.sleep(1.0)

asyncio.get_event_loop().run_until_complete(gps_stream())
